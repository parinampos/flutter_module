import 'package:flutter_module/dashboard/DragDropItem.dart';
import 'package:flutter_module/dashboard/dragdrop_item_detail_widget.dart';
import 'package:flutter_module/dashboard/dragdrop_item_header_widget.dart';
import 'package:flutter_module/style/AmposColors.dart';
import 'package:flutter_module/style/textstyle/DashboardTextStyle.dart';
import 'package:flutter/material.dart';

class DragDropItemWidget extends StatelessWidget {
  DragDropItem item;

  DragDropItemWidget(DragDropItem item) {
    this.item = item;
  }

  @override
  Widget build(BuildContext context) {
    return buildCard();
  }

  Widget buildCard() {
    return Card(
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/dashboard_item/stars_elements.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: [
                Column(
                  children: [
                    DragDropItemHeaderWidget(item),
                    buildImage(),
                    DragDropItemDetailWidget(item),
                  ],
                ),
                buildMessage()
              ],
            )));
  }

  Widget buildImage() {
    return Container(
      margin: EdgeInsets.all(16),
      child: Image.asset(
        item.imagePath,
        width: 60,
        height: 60,
      ),
    );
  }

  Widget buildMessage() {
    if (item.message.isEmpty) {
      return Container();
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.fromLTRB(0, 40, 4, 0),
              decoration: BoxDecoration(
                gradient: RadialGradient(
                  center: const Alignment(0, 0),
                  radius: 0.5,
                  colors: [
                    AmposColors.deepCarrotOrange,
                    AmposColors.transparent,
                  ],
                  stops: [1.0, 1.0],
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    item.message,
                    textAlign: TextAlign.center,
                    style: DashboardTextStyle.messageTextStyle,
                  ),
                ],
              ))
        ],
      );
    }
  }
}
