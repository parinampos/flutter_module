import 'dart:async';

import 'package:flutter/services.dart';

class PlatformRouter {
  static const _platform = const MethodChannel('com.am_flutter/router');

  Future<void> gotoAnnouncements() async {
    try {
      await _platform.invokeMethod('gotoAnnouncements');
    } on PlatformException catch (_) {
      throw 'Unable to goto announcement screen.';
    }
  }

  Future<void> gotoAnnouncementDetail(int id) async {
    try {
      await _platform.invokeMethod('gotoAnnouncementDetail', <String, dynamic> {
        'id': id,
      });
    } on PlatformException catch (_) {
      throw 'Unable to goto announcement detail for id: $id.';
    }
  }

}
