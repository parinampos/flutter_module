import 'package:flutter_module/domain/entity/dashboard.dart';
import 'package:flutter_module/data/dashboard_api.dart';

import 'package:http/http.dart' as http;

class DashboardInteractor {

  http.Client client;

  DashboardInteractor(http.Client client) {
    this.client = client;
  }

  Future<Dashboard> fetchDashboard() {
    final api = DashboardApi();
    return api.fetchDashboard(client);
  }
}
