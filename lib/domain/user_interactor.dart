import 'package:flutter_module/domain/entity/user.dart';
import 'package:flutter_module/platform_data_provider.dart';

class UserInteractor {
  final _platformDataProvider = PlatformDataProvider();

  Future<User> getUser() async {
    return _platformDataProvider.getPlatformData('getUserData').then((map) {
      final user = User(name: map['name'].toString());
      return user;
    });
  }
}
