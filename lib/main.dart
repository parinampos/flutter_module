import 'package:flutter_module/dashboard/dragdrop_widget.dart';
import 'package:flutter_module/dashboard/highlight_widget.dart';
import 'package:flutter_module/dashboard/my_team_item_widget.dart';
import 'package:flutter_module/domain/entity/dashboard.dart';
import 'package:flutter_module/domain/entity/user.dart';
import 'package:flutter_module/domain/user_interactor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_module/style/AmposColors.dart';

import 'package:http/http.dart' as http;

import 'dashboard/header_widget.dart';
import 'domain/dashboard_interactor.dart';
import 'platform_router.dart';

void main() {
  debugPaintSizeEnabled = false;
  final dashboardInteractor = DashboardInteractor(http.Client());
  final userInteractor = UserInteractor();
  runApp(MyApp(
      dashboard: dashboardInteractor.fetchDashboard(),
      user: userInteractor.getUser(),
  ));
}

class MyApp extends StatelessWidget {

  final PlatformRouter _platformRouter = PlatformRouter();
  final Future<Dashboard> dashboard;
  final Future<User> user;

  MyApp({Key key, @required this.dashboard, @required this.user});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          backgroundColor: Color(0xFFACDEDF),
          drawer: _buildDrawer(context),
          body: SafeArea(child: buildCustomScroll()),
        ));
  }

  Widget buildCustomScroll() {
    return CustomScrollView(slivers: <Widget>[
      SliverAppBar(
        backgroundColor: AmposColors.transparent,
        pinned: true,
        expandedHeight: 240.0,
        flexibleSpace: FlexibleSpaceBar(
          background: HeaderWidget(
            dashboard: dashboard,
            user: user,
          ),
        ),
      ),
      SliverPadding(
//        padding: EdgeInsets.all(8),

        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        sliver: SliverList(
            delegate: SliverChildListDelegate(
              [
                _buildHighlightSection(),
                MyTeamItemWidget(),
              ],
            )),
      ),
      SliverPadding(
//        padding: EdgeInsets.all(8),
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        sliver: DragDropWidget(dashboard: dashboard),
      )
    ]);
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Oscar'),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          ListTile(
            title: Text('View Announcements'),
            onTap: () {
              _platformRouter.gotoAnnouncements();
            },
          ),
          ListTile(
            title: Text('Announcement 18'),
            onTap: () {
              _platformRouter.gotoAnnouncementDetail(18);
            },
          )
        ],
      ),
    );
  }

  Widget _buildHighlightSection() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
            child: Text(
              'HIGHLIGHT',
              style: TextStyle(
                  color: Color(0xFF0B3051),
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0),
            ),
          ),
          HighlightWidget(dashboard: dashboard)
        ],
      ),
    );
  }
}
