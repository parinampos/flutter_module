import 'package:flutter_module/data/dashboard_api.dart';
import 'package:flutter_module/domain/entity/dashboard.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import "package:flutter_test/flutter_test.dart";

class MockClient extends Mock implements http.Client {}

void main() {
  var client = MockClient();
  var dashboardService = DashboardApi();
  var mockResponse = '{"bannerUrl":"http://countryside-landscape.com/files/2018/08/september-banner.jpg","highlight":[{"feature":"Announcement","title":"good working environement","imageUrl":"https://cdn4.iconfinder.com/data/icons/business-marketing-and-management-round/128/1-512.png","hasNewNotification":true},{"feature":"Survey","title":"Company Trip Japan","imageUrl":"https://cdn2.iconfinder.com/data/icons/digital-and-internet-marketing-3-1/50/111-512.png","hasNewNotification":true},{"feature":"Learning","title":"APP Release Timeline","imageUrl":"https://www.clipartmax.com/png/middle/171-1715896_paper-book-icon-textbook-icon.png","hasNewNotification":false},{"feature":"Reward","title":"New Redemption Product!","imageUrl":"https://cdn3.iconfinder.com/data/icons/discount-and-promotion/500/gift-512.png","hasNewNotification":true},{"feature":"Assignment","title":"Dont forget to submit","imageUrl":"https://cdn4.iconfinder.com/data/icons/education-training/33/homework-512.png","hasNewNotification":false},{"feature":"Coaching","title":"New coach Solskjaer!!","imageUrl":"https://i.guim.co.uk/img/static/sys-images/Football/Pix/pictures/2001/12/13/SolskjaerPAbg.jpg?width=300\u0026quality=85\u0026auto=format\u0026fit=max\u0026s=94e88f4b2b0836184cd33ade9fe9490a","hasNewNotification":false}],"reward":{"receivedStar":47},"learning":{"assigned":3,"open":4,"inProgress":10},"announcement":{"new":6},"messaging":{"new":24},"coaching":{"today":2,"missed":1},"assignment":{"new":3},"survey":{"new":2},"send_star":{"remaining_quota":9},"classroom_training":{"today":1}}';
  
  group('test retrieve data', () {
    test('throws an exception when fetch with 404 not found', () {
      when(client.get('https://api.myjson.com/bins/1chtqc'))
          .thenAnswer((_) async => http.Response('Not found', 404)
      );

      expect(dashboardService.fetchDashboard(client), throwsException);
    });

    test('returns dashboard data when fetch correctly', () async {
      when(client.get('https://api.myjson.com/bins/1chtqc'))
          .thenAnswer((_) async => http.Response(
          mockResponse, 200)
      );

      var dashboard = await dashboardService.fetchDashboard(client);
      
      expect(dashboard, isInstanceOf<Dashboard>());
    });
  });
  
  group('test highlights', () {
    when(client.get('https://api.myjson.com/bins/1chtqc'))
        .thenAnswer((_) async => http.Response(
        mockResponse, 200)
    );
    
    test('return numbers of highlight correctly', () async {
      var dashboard = await dashboardService.fetchDashboard(client);
      var highlights = dashboard.highlights;
      
      expect(highlights.length, 6);
    });

    test('return unread count correctly', () async {
      var dashboard = await dashboardService.fetchDashboard(client);
      var highlights = dashboard.highlights;

      expect(highlights[0].hasNewNotification, true);
      expect(highlights[1].hasNewNotification, true);
      expect(highlights[2].hasNewNotification, false);
      expect(highlights[3].hasNewNotification, true);
      expect(highlights[4].hasNewNotification, false);
      expect(highlights[5].hasNewNotification, false);
    });
  });

  group('test widgets', () {
    when(client.get('https://api.myjson.com/bins/1chtqc'))
        .thenAnswer((_) async => http.Response(
        mockResponse, 200)
    );

    test('return updated count of each module correctly', () async {
      var dashboard = await dashboardService.fetchDashboard(client);

      expect(dashboard.reward.receivedStar, 47);
      expect(dashboard.announcement.newAnnouncement, 6);
      expect(dashboard.messaging.newMessage, 24);
      expect(dashboard.coaching.today, 2);
      expect(dashboard.coaching.missed, 1);
      expect(dashboard.assignment.newAssignment, 3);
      expect(dashboard.survey.newSurvey, 2);
      expect(dashboard.sendStar.remainingQuota, 9);
      expect(dashboard.classroomTraining.today, 1);
    });
  });
}
